import math, os, sys
import numpy as np
#import matplotlib.pyplot as plt
import pandas as pd
import json
import sqlite3
import ast
from collections import deque

import torch as T
from torch.autograd import Variable
import torch.nn.utils as utils

from bokeh.plotting import figure, show
from bokeh.io import output_notebook
from bokeh.palettes import Spectral11
output_notebook()

import sklearn
import sklearn.preprocessing

sys.path.append('../../environments/')

from ddpg import DDPGagent
import env_sde

path = '../data/ddpg_model/'


def optimal_sampling(num_episodes=100,scale=2.0):
    fdict = {}
    xp = np.linspace(-2.5,2.5,1000)
    dv,v = env.get_solution(n_points=1001,plots=False)
    scores_window = deque(maxlen=100)
    scores = []
    avg_score = []
    steps=[]
    for i_episode in range(num_episodes+2):
        done = False
        score = 0
        observation = env.reset()
        step = 0
        scale=scale
        while not done:
            step +=1
            tmp = np.argmin((xp-observation.item())**2)
            action = dv[tmp]
            observation, reward, done, info = env.step(scale*(-1)*action.item())
            score += reward
        scores.append(score)
        scores_window.append(score)
        avg_score.append(np.mean(scores_window))
        steps.append(step)

    fdict['reward'] = scores
    fdict['avg_reward'] = avg_score
    fdict['steps'] = steps
    fdict['algo'] = 'optimal'
    fdict['beta'] = env.beta
    return fdict


def get_scaler(env):
    state_space_samples = np.linspace(env.min_position,env.max_position,1000).reshape(-1,1)  #returns shape =(1,1)
    #state_space_samples = np.array([ob_space.sample() for x in range(10000)])
    scaler = sklearn.preprocessing.StandardScaler()
    scaler.fit(state_space_samples)

    return scaler

#function to normalize states
def scale_state(state,scaler):                  #requires input shape=(1,)
    scaled = scaler.transform(state)
    return scaled


def get_plots(data):
    if data['algo'] == 'reinforce':
        title = data['name']+' Algo: '+data['algo']+' beta: '+str(data['beta'])+' stop: '+str(data['stop'])+' lrate: '+str(data['lrate'])
        xp, dv, ap = plot_reinforce_solution(data)
        data['space'] = xp; data['pde'] = dv; data['control'] = ap
    elif data['algo'] == 'reinforce2':
        title = data['name']+' Algo: '+data['algo']+' beta: '+str(data['beta'])+' stop: '+str(data['stop'])+' lrate: '+str(data['lrate'])+'net_size:' +str(data['net_size'])
        xp, dv, ap = plot_reinforce_traj_solution(data)
        data['space'] = xp; data['pde'] = dv; data['control'] = ap
    elif data['algo'] == 'cem':
        title = data['name']+' Algo: '+data['algo']+' beta: '+str(data['beta'])+' stop: '+str(data['stop'])+' net_size: '+str(data['net_size'])+' pop_size: '+str(data['pop_size'])+' elite_frac: '+str(data['elite_frac'])+' sigma: '+str(data['sigma'])
        xp, dv, ap = plot_cem_solution(data)
        data['space'] = xp; data['pde'] = dv; data['control'] = ap
    elif data['algo'] == 'ddpg':
        title = data['name']+' Algo: '+data['algo']+' beta: '+str(data['beta'])+' stop: '+str(data['stop'])+' net_size: '+str(data['net_size'])+' lrate_actor: ' +str(data['lrate_actor'])+' lrate_critic: '+str(data['lrate_critic'])
        xp, dv, ap_end, ap_start = plot_ddpg_solution(data)
        data['space'] = xp; data['pde'] = dv; data['control'] = ap_end; data['init'] = ap_start
    elif data['algo'] == 'ac':
        title = data['name']+' Algo: '+data['algo']+' beta: '+str(data['beta'])+' stop: '+str(data['stop'])+' net_size_layer1: '+str(data['net_size_layer1'])+'net_size_layer2: '+str(data['net_size_layer2'])+' lrate_actor: ' +str(data['lrate_actor'])+' lrate_critic: '+str(data['lrate_critic'])
        xp, dv, ap = plot_ac_solution(data)
        data['space'] = xp; data['pde'] = dv; data['control'] = ap
    elif data['algo'] == 'optimal':
        title = 'Sampling with optimal control'
        xp, dv, ap = plot_optimal_solution(data)
        data['space'] = xp; data['pde'] = dv; #data['control'] = ap
    p = figure(title=title,x_axis_label='Trajectories',y_axis_label='Reward',plot_width=800,plot_height=800)
    p.line(list(range(1,len(data['reward'])+1)),data['reward'],color='blue')
    p.line(list(range(1,len(data['avg_reward'])+1)),data['avg_reward'],color='red')
    show(p)

    p = figure(title=title,x_axis_label='Trajectories',y_axis_label='Steps',plot_width=800,plot_height=800)
    p.line(list(range(1,len(data['steps'])+1)),data['steps'],color='blue')
    show(p)

    if 'space' in data.keys():
        p = figure(title='Compare solution',x_axis_label='x space',y_axis_label='Function space',plot_width=800,plot_height=800)
        p.line(data['space'][1:],data['pde'],color='blue')
        if 'control' in data.keys():
            p.line(data['space'],data['control'],color='red')
            p.line(data['space'],data['init'],color='green')
        show(p)

def plot_experiemnts(algo):
    conn = sqlite3.connect('../data/sde_ddpg_experiments.db')
    c = conn.cursor()
    c.execute('''SELECT * FROM {}'''.format(algo))
    data = c.fetchall()
    desc = c.description
    name = [cols[0] for cols in c.description]
    tmp = {}
    for d in data:
        for i,elm in enumerate(name):
            if type(d[i]) is str:
                if d[i][0] == '[':
                    tmp[elm] = ast.literal_eval(d[i])
                else:
                    tmp[elm] = d[i]
            else:
                tmp[elm] = d[i]
        get_plots(tmp)
    conn.close()

def plot_reinforce_solution(data):
    env = sde_gym.Double_Well(x0=-1.0,beta=data['beta'],seed=100)
    agent = REINFORCE(env.observation_space_shape, env.action_space, hidden_size=data['net_size'], alpha=data['lrate'])
    scaler = get_scaler(env)
    agent.model.load_state_dict(T.load('ckpt_SDE_Double_Well_old/reinforce_model/SDE_Double_Well_2.0_reinforce_128_0.00000100_2.0.pkl'))
    xp = np.linspace(-2.5,2.5,100)
    dv,v = env.get_solution(n_points=100,plots=False)
    ap = np.zeros((10,100))
    for i in range(10):
        with T.no_grad():
            for j,x in enumerate(xp):
                action, _, _ = agent.select_action(T.tensor([float(scale_state(x.reshape(1,-1),scaler).item())]))
                ap[i,j] = action

    return xp, dv, ap.mean(axis=0)

def plot_reinforce_traj_solution(data):# not finished!
    env = env_sde.Double_Well(x0=-1.0,beta=data['beta'],seed=100)
    agent = REINFORCE_TRAJ(hidden_size=data['net_size'], num_inputs=env.observation_space_shape, action_space=env.action_space,alpha=data['lrate'])
    scaler = get_scaler(env)
    agent.model.load_state_dict(T.load('ckpt_SDE_Double_Well/reinforce_traj_model/{}'.format(data['filename']), map_location=T.device('cpu')))
    xp = np.linspace(-2.5,2.5,100)
    dv,v = env.get_solution(n_points=100,plots=False)
    ap = np.zeros((10,100))
    for i in range(10):
        with T.no_grad():
            for j,x in enumerate(xp):
                action, _, _ = agent.select_action(T.tensor([float(scale_state(x.reshape(1,-1),scaler).item())]))
                ap[i,j] = action

    return xp, dv, ap.mean(axis=0)

def plot_cem_solution(data):
    env = env_sde.Double_Well(x0=-1.0,beta=data['beta'],seed=100)
    agent = cem_agent.Agent(env)
    agent.load_state_dict(T.load('ckpt_SDE_Double_Well/cem_model/{}.pkl'.format(data['filename'][:-5])))
    agent.eval()
    xp = np.linspace(-2.5,2.5,100)
    dv,v = env.get_solution(n_points=100,plots=False)
    device='cpu'
    with T.no_grad():
        ap = [agent(T.from_numpy(np.array([x])).float().to(device)).item() for x in xp]
    return xp, dv, ap

def plot_ac_solution(data):
    env = env_sde.Double_Well(x0=-1.0,beta=data['beta'],seed=100)
    xp = np.linspace(-2.5,2.5,100)
    dv,v = env.get_solution(n_points=100,plots=False)
    agent = Agent(alpha=data['lrate_actor'], beta=data['lrate_critic'], input_dims=[1], gamma=1.0, layer1_size=data['net_size_layer1'], layer2_size=data['net_size_layer2'])
    agent.critic.load_state_dict(T.load('ckpt_SDE_Double_Well/ac_model/{}_a2c-critic.pkl'.format(data['filename'][:-5]),map_location=T.device('cpu')))
    agent.actor.load_state_dict(T.load('ckpt_SDE_Double_Well/ac_model/{}_a2c-actor.pkl'.format(data['filename'][:-5]),map_location=T.device('cpu')))
    with T.no_grad():
        ap =[agent.choose_action(np.array([(x).item()])) for x in xp]

    return xp, dv, ap

def plot_ddpg_solution(data):
    env = env_sde.Double_Well(x0=-1.0,beta=data['beta'],seed=100)
    xp = np.linspace(-2.5,2.5,100)
    dv,v = env.get_solution(n_points=100,plots=False)
    scaler = get_scaler(env)
    agent_end = DDPGagent(env,hidden_size=data['net_size'])
    agent_end.critic.load_state_dict(T.load(os.path.join(path,'{}_ddpg-critic-last.pkl'.format(data['filename'][:-5]))))
    agent_end.actor.load_state_dict(T.load(os.path.join(os.path.join(path,'{}_ddpg-actor-last.pkl'.format(data['filename'][:-5])))))
    with T.no_grad():
        ap_end = [agent_end.get_action(scale_state((x).reshape(1,-1),scaler)).item() for x in xp]

    agent_start = DDPGagent(env,hidden_size=data['net_size'])
    agent_start.critic.load_state_dict(T.load(os.path.join(path,'{}_ddpg-critic-start.pkl'.format(data['filename'][:-5]))))
    agent_start.actor.load_state_dict(T.load(os.path.join(path,'{}_ddpg-actor-start.pkl'.format(data['filename'][:-5]))))
    with T.no_grad():
        ap_start = [agent_start.get_action(scale_state((x).reshape(1,-1),scaler)).item() for x in xp]


    return xp, dv, ap_end, ap_start

def plot_optimal_solution(data):
    env = env_sde.Double_Well(x0=-1.0,beta=data['beta'],seed=100)
    xp = np.linspace(-2.5,2.5,100)
    dv,v = env.get_solution(n_points=100,plots=False)
    ap = 0
    return xp, dv, ap

def get_optimal_sampling():
    lst = [0.5,1.0,2.0,4.0,10.0]
    print('                      E[R] \t V[R] \t E[steps] \t V[steps]')
    for l in lst:
        env = env_sde.Double_Well(x0=-1.0,beta=2.0,seed=100)
        res = optimal_sampling(num_episodes=5000,scale=l)
        print('Stats for scale {}: {:4.4f} \t {:4.4f} \t {:4.4f} \t {:4.4f} '.format(l,np.mean(res['reward']),np.var(res['reward']),np.mean(res['steps']),np.var(res['steps'])))

def plot_potential():
    xp = np.linspace(-2,2,1000)
    V =lambda x: 1/2*(x**2-1)**2
    p = figure(title='Potential',x_axis_label='x',y_axis_label='V',plot_width=800,plot_height=800)
    p.line(xp,V(xp),color='blue',line_width=4)
    show(p)

def get_optimal_plot():
    env = env_sde.Double_Well(x0=-1.0,beta=2.0,seed=100)
    res = optimal_sampling()
    get_plots(res)
