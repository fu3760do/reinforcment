#!/bin/bash
#coding: uft-8

# make folder
folder=../results_HPC/ddpg/"$1"
# make folder and change into it
mkdir $folder
cd $folder

# get data from fu curta
scp fu3760do@curta.zedat.fu-berlin.de:/scratch/fu3760do/rl/ddpg/"$1".tar.gz .

# unpack data into existing folders
tar -xf ./"$1".tar.gz

# include data into database
python ../../../src/json2db.py

