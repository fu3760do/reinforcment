from pathlib import Path
import sys
import json
import itertools

import visual_ddpg as vis
sys.path.append('../')
from data_model import db_session
from data_model.db_session import SessionContext
from data_model.data_model import DdpgSde2d as DdpgSde


def setup_db():
    db_file = Path(__file__).parent.parent / 'db' / 'ddpg_sde_experiments.sqlite'
    print(str(db_file))
    db_session.global_init(str(db_file))


def get_data(net_size) -> None:
    """
    lrates = [1e-3, 1e-4, 1e-5, 1e-6]
    rngs = [21, 42, 84, 126, 168]
    para = list(itertools.product(lrates, lrates))
    for lrate in para:
        print(f'Learn_rates: {lrate}')
        for rng in rngs:
            #elm = ctx.session.query(DdpgSde).filter(DdpgSde.net_size == net_size,
            #                                        DdpgSde.learn_rate_critic == lrate[0],
            #                                        DdpgSde.learn_rate_actor == lrate[1],
            #                                        DdpgSde.rng == rng).first()
    """

    with SessionContext() as ctx:
        print('Plotting SDE beta = 4.0 stop = -4.0')
        query = ctx.session.query(DdpgSde).all()
        #query = ctx.session.query(DdpgSde).filter(DdpgSde.net_size == net_size).all()
        print(f'There are {len(query)} files with this netsize {net_size} in this database.')

        for elm in query:
            tmp = elm.__dict__
            tmp['reward'] = json.loads(tmp['reward'])
            tmp['avg_reward'] = json.loads(tmp['avg_reward'])
            tmp['steps'] = json.loads(tmp['steps'])
            tmp['l2error'] = json.loads(tmp['l2error'])
            vis.get_plots2d(tmp)


if __name__ == '__main__':
    setup_db()
    get_data(32)
