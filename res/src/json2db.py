"""Script for uploading data into the database"""
from pathlib import Path
import sys
import json
sys.path.append('../')
from data_model import db_session
from data_model.db_session import SessionContext
from data_model.data_model import DdpgSde1d as DdpgSde


def setup_db():
    db_file = Path(__file__).parent.parent / 'db' / 'ddpg_sde_experiments.sqlite'
    db_session.global_init(str(db_file))


def record_db(f: dict):
    with SessionContext(commit_on_success=True) as ctx:
        data = DdpgSde(filename=f['filename'],
                       name=f['name'],
                       algo=f['algo'],
                       beta=f['beta'],
                       stop=f['stop'],
                       net_size=f['net_size'],
                       rng=f['rng'],
                       learn_rate_actor=f['lrate_actor'],
                       learn_rate_critic=f['lrate_critic'],
                       reward=json.dumps(f['reward']),
                       avg_reward=json.dumps(f['avg_reward']),
                       steps=json.dumps(f['step']),
                       l2error=json.dumps(f['l2_error']),
                       success=f['sucess'],
                       maxlen=f['max_len'],
                       )
        print(data)
        ctx.session.add(data)


def insert_db():
    path = Path(__file__).parent.parent/'results_HPC'/'ddpg'/'exp230322'/'ddpg_result'
    p = path.glob('*.json')
    files = [x for x in p if x.is_file()]
    for file in files:
        data = json.load(open(file))
        data['filename'] = str(file).split('/')[-1]
        print(data['filename'])
        record_db(data)


def get_data():
    with SessionContext() as ctx:
        data = ctx.session.query(DdpgSde).filter(DdpgSde.algo == 'ddpg')
        #data.column_descriptions
        for elm in data:
            print(elm.filename)


if __name__ == '__main__':
    setup_db()
    insert_db()
    #get_data()
