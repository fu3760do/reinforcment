import sqlalchemy as sa
import sqlalchemy.orm as orm
from sqlalchemy.orm import Session
import sys

sys.path.append('../')
from data_model.base_class import SqlAlchemyBase

__factory = None


def global_init(db_file: str):
    global __factory

    if __factory:
        return

    if not db_file or not db_file.strip():
        raise Exception("You must provide a df file.")

    conn_str = 'sqlite:///' + db_file.strip()
    engine = sa.create_engine(conn_str, echo=False)
    __factory = orm.sessionmaker(bind=engine)

    import data_model.__all_models
    SqlAlchemyBase.metadata.create_all(engine)


def create_session() -> Session:
    global __factory
    session: Session = __factory()
    session.expire_on_commit = False
    return session


class SessionContext:
    def __init__(self, commit_on_success=False):
        self.commit_on_success: bool = commit_on_success
        self.session = create_session()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if not exc_val and self.commit_on_success:
            self.session.commit()

        self.session.close()
