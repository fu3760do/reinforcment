import sqlalchemy as sa
import datetime

from data_model.base_class import SqlAlchemyBase


# can be used for ac aswell
class DdpgSde1d(SqlAlchemyBase):
    __tablename__ = 'ddpg_sde'

    filename = sa.Column(sa.String, primary_key=True)
    name = sa.Column(sa.String)
    created = sa.Column(sa.DateTime, default=datetime.datetime.now, index=True)
    algo = sa.Column(sa.String)
    beta = sa.Column(sa.REAL)
    stop = sa.Column(sa.REAL)
    rng = sa.Column(sa.Integer)
    net_size = sa.Column(sa.Integer)
    learn_rate_actor = sa.Column(sa.REAL)
    learn_rate_critic = sa.Column(sa.REAL)
    success = sa.Column(sa.Boolean)
    maxlen = sa.Column(sa.Boolean)
    reward = sa.Column(sa.String)
    avg_reward = sa.Column(sa.String)
    steps = sa.Column(sa.String)
    l2error = sa.Column(sa.String)


# do we really need that?
class DdpgSde2d(SqlAlchemyBase):
    __tablename__ = 'ddpg_sde_2d'

    filename = sa.Column(sa.String, primary_key=True)
    name = sa.Column(sa.String)
    created = sa.Column(sa.DateTime, default=datetime.datetime.now, index=True)
    algo = sa.Column(sa.String)
    beta = sa.Column(sa.REAL)
    stop = sa.Column(sa.REAL)
    rng = sa.Column(sa.Integer)
    net_size = sa.Column(sa.Integer)
    learn_rate_actor = sa.Column(sa.REAL)
    learn_rate_critic = sa.Column(sa.REAL)
    success = sa.Column(sa.Boolean)
    maxlen = sa.Column(sa.Boolean)
    reward = sa.Column(sa.String)
    avg_reward = sa.Column(sa.String)
    steps = sa.Column(sa.String)
    l2error = sa.Column(sa.String)


class CemSde1d(SqlAlchemyBase):
    __tablename__ = 'cem_sde'

    filename = sa.Column(sa.String, primary_key=True)
    name = sa.Column(sa.String)
    created = sa.Column(sa.DateTime, default=datetime.datetime.now, index=True)
    algo = sa.Column(sa.String)
    beta = sa.Column(sa.REAL)
    stop = sa.Column(sa.REAL)
    rng = sa.Column(sa.Integer)
    net_size = sa.Column(sa.Integer)
    pop_size = sa.Column(sa.Integer)
    elite_frac = sa.Column(sa.REAL)
    success = sa.Column(sa.Boolean)
    reward = sa.Column(sa.String)
    avg_reward = sa.Column(sa.String)
    steps = sa.Column(sa.String)


class reinforce1d(SqlAlchemyBase):
    __tablename__ = 'ddpg_sde'

    filename = sa.Column(sa.String, primary_key=True)
    name = sa.Column(sa.String)
    created = sa.Column(sa.DateTime, default=datetime.datetime.now, index=True)
    algo = sa.Column(sa.String)
    beta = sa.Column(sa.REAL)
    stop = sa.Column(sa.REAL)
    rng = sa.Column(sa.Integer)
    net_size = sa.Column(sa.Integer)
    learn_rate = sa.Column(sa.REAL)
    success = sa.Column(sa.Boolean)
    maxlen = sa.Column(sa.Boolean)
    reward = sa.Column(sa.String)
    avg_reward = sa.Column(sa.String)
    steps = sa.Column(sa.String)
    l2error = sa.Column(sa.String)



